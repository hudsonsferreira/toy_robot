# README

Toy Robot is a solution for [Toy Robot](https://github.com/askcharlie/toy_robot) challenge.

## Dependency
 * Ruby 2.4.0 - You can use [rbenv](https://github.com/rbenv/rbenv)

## Setup

```
$ git clone git@bitbucket.org:hudsonsferreira/toy_robot.git
$ bundle install
```

## Running specs

`$ rspec`

## Usage

```ruby
$ pry

> require_relative 'lib/commander'
> commander = Commander.new
> commander.execute('spec/fixtures/example_one.txt')

=> "Output: 0,1,NORTH"
```
