require './lib/table'

RSpec.describe Table do
  describe 'constants' do
    it { expect(described_class::BOUND).to eq 4 }
  end

  describe '.valid_movement?' do
    let(:table) { described_class.new }

    context 'valid' do
      it 'returns true for bound values' do
        expect(described_class.valid_movement?(2, 3)).to be_truthy

        expect(described_class.valid_movement?(4, 3)).to be_truthy
      end
    end

    context 'invalid' do
      it 'returns false for negative values' do
        expect(described_class.valid_movement?(-1, 3)).to be_falsey
      end

      it 'returns false beyond bound values' do
        expect(described_class.valid_movement?(5, 3)).to be_falsey
      end
    end
  end

  describe '#initialize' do
    it 'table coordinates must be on (0,0) SOUTH WEST corner' do
      table = described_class.new

      expect(table.x).to be_zero
      expect(table.y).to be_zero
    end
  end
end
