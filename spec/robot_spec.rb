require './lib/robot'
require './lib/table'

RSpec.describe Robot do
  describe '#on_table_surface?' do
    let(:robot) { described_class.new }

    context 'when it is' do
      it 'returns true' do
        table = Table.new
        robot.table = table

        expect(robot.on_table_surface?).to be_truthy
      end
    end

    context 'when it is not' do
      it 'returns false' do
        expect(robot.on_table_surface?).to be_falsey
      end
    end
  end

  describe '#report' do
    it 'returns current position output' do
      table = Table.new
      table.x = 2
      table.y = 3

      robot = described_class.new
      robot.table = table
      robot.direction = 'NORTH'

      expect(robot.report).to eq 'Output: 2,3,NORTH'
    end
  end

  describe '#place' do
    let(:robot) { Robot.new }

    before { robot.place 2,4,'EAST' }

    it 'assigns table coordinates values'do
      expect(robot.table.x).to eq 2
      expect(robot.table.y).to eq 4
    end

    it 'assigns robot direction value' do
      expect(robot.direction).to eq 'EAST'
    end
  end

  describe '#move' do
    let(:robot) { Robot.new }

    context 'when robot#direction is EAST' do
      before { robot.place 2,4,'EAST' }

      it 'moves robot one more square to EAST' do
        robot.move

        expect(robot.table.x).to eq 3
      end
    end

    context 'when robot#direction is NORTH' do
      before { robot.place 2,3,'NORTH' }

      it 'moves robot one more square to NORTH' do
        robot.move

        expect(robot.table.y).to eq 4
      end
    end

    context 'when robot#direction is SOUTH' do
      before { robot.place 2,3,'SOUTH' }

      it 'moves robot one more square to SOUTH' do
        robot.move

        expect(robot.table.y).to eq 2
      end
    end

    context 'when robot#direction is WEST' do
      before { robot.place 2,3,'WEST' }

      it 'moves robot one more square to WEST' do
        robot.move

        expect(robot.table.x).to eq 1
      end
    end
  end

  describe '#left' do
    let(:robot) { Robot.new }

    context 'when robot#direction is EAST' do
      before { robot.place 2,4,'EAST' }

      it 'moves robot#direction to NORTH' do
        robot.left

        expect(robot.direction).to eq 'NORTH'
      end
    end

    context 'when robot#direction is NORTH' do
      before { robot.place 2,3,'NORTH' }

      it 'moves robot#direction to WEST' do
        robot.left

        expect(robot.direction).to eq 'WEST'
      end
    end

    context 'when robot#direction is SOUTH' do
      before { robot.place 2,3,'SOUTH' }

      it 'moves robot#direction to EAST' do
        robot.left

        expect(robot.direction).to eq 'EAST'
      end
    end

    context 'when robot#direction is WEST' do
      before { robot.place 2,3,'WEST' }

      it 'moves robot#direction to SOUTH' do
        robot.left

        expect(robot.direction).to eq 'SOUTH'
      end
    end
  end

  describe '#right' do
    let(:robot) { Robot.new }

    context 'when robot#direction is EAST' do
      before { robot.place 2,4,'EAST' }

      it 'moves robot#direction to SOUTH' do
        robot.right

        expect(robot.direction).to eq 'SOUTH'
      end
    end

    context 'when robot#direction is NORTH' do
      before { robot.place 2,3,'NORTH' }

      it 'moves robot#direction to EAST' do
        robot.right

        expect(robot.direction).to eq 'EAST'
      end
    end

    context 'when robot#direction is SOUTH' do
      before { robot.place 2,3,'SOUTH' }

      it 'moves robot#direction to WEST' do
        robot.right

        expect(robot.direction).to eq 'WEST'
      end
    end

    context 'when robot#direction is WEST' do
      before { robot.place 2,3,'WEST' }

      it 'moves robot#direction to NORTH' do
        robot.right

        expect(robot.direction).to eq 'NORTH'
      end
    end
  end
end
