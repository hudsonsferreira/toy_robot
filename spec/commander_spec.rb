require './lib/commander'

RSpec.describe Commander do
  describe '#initialize' do
    let(:commander) { described_class.new }

    it 'assigns commander#robot' do
      expect(commander.robot).to be_instance_of Robot
    end
  end

  describe '#execute' do
    let(:commander) { described_class.new }

    context 'example one' do
      it 'returns Output: 0,1,NORTH' do
        output = commander.execute('spec/fixtures/example_one.txt')

        expect(output).to eq 'Output: 0,1,NORTH'
      end
    end

    context 'example two' do
      it 'returns Output: 0,2,WEST' do
        output = commander.execute('spec/fixtures/example_two.txt')

        expect(output).to eq 'Output: 0,2,WEST'
      end
    end

    context 'example three' do
      it 'returns Output: 0,4,SOUTH' do
        output = commander.execute('spec/fixtures/example_three.txt')

        expect(output).to eq 'Output: 0,4,SOUTH'
      end
    end

    context 'example four' do
      it 'returns Output: 2,4,SOUTH' do
        output = commander.execute('spec/fixtures/example_four.txt')

        expect(output).to eq 'Output: 2,4,SOUTH'
      end
    end

    context 'example five' do
      it 'returns Output: 4,4,EAST' do
        output = commander.execute('spec/fixtures/example_five.txt')

        expect(output).to eq 'Output: 4,4,EAST'
      end
    end
  end
end
