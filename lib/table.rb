class Table
  BOUND = 4
  attr_accessor :x, :y

  def self.valid_movement? desired_x, desired_y
    (0..BOUND).include?(desired_x) && (0..BOUND).include?(desired_y)
  end

  def initialize
    @x = 0
    @y = 0
  end
end
