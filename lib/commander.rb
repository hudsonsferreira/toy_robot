require_relative 'robot'
require_relative 'table'

class Commander
  attr_reader :robot

  def initialize
    @robot = Robot.new
  end

  def execute(file_path)
    File.foreach(file_path) do |command|
      formatted_command = command.strip.split(' ')
      main_command = formatted_command[0].downcase!

      case main_command
      when 'place'
        args = formatted_command[1].split(',')
        robot.send(main_command, args[0].to_i, args[1].to_i, args[2])
      when 'report'
        return robot.send(main_command)
      else
        robot.send(main_command)
      end
    end
  end
end
