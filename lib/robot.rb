class Robot
  attr_accessor :table, :direction

  def place(desired_x, desired_y, desired_direction)
    if Table.valid_movement?(desired_x, desired_y)
      self.table ||= Table.new

      self.table.x = desired_x
      self.table.y = desired_y

      self.direction = desired_direction
    end
  end

  def move
    return unless on_table_surface?

    case direction
    when 'NORTH'
      place(table.x, table.y + 1, direction)
    when 'SOUTH'
      place(table.x, table.y - 1, direction)
    when 'EAST'
      place(table.x + 1, table.y, direction)
    when 'WEST'
      place(table.x - 1, table.y, direction)
    end
  end

  def left
    return unless on_table_surface?

    case self.direction
    when 'NORTH'
      self.direction = 'WEST'
    when 'SOUTH'
      self.direction = 'EAST'
    when 'EAST'
      self.direction = 'NORTH'
    when 'WEST'
      self.direction = 'SOUTH'
    end
  end

  def right
    return unless on_table_surface?

    case self.direction
    when 'NORTH'
      self.direction = 'EAST'
    when 'SOUTH'
      self.direction = 'WEST'
    when 'EAST'
      self.direction = 'SOUTH'
    when 'WEST'
      self.direction = 'NORTH'
    end
  end

  def report
    %Q(Output: #{table.x},#{table.y},#{direction}) if on_table_surface?
  end

  def on_table_surface?
    table
  end
end
